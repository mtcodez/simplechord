package cs455.overlay.transport;

import cs455.overlay.node.Registry;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

/**
 * Created by mattluo on 2/6/15.
 * Provide the basic functionality to receive data
 * from a TCP connection. It has a thread running in order
 * to listen for incoming connections.
 */
public class TCPReceiverThread extends Thread {
    private Socket socket;
    private DataInputStream din;
    // For Callback
    private TCPReceiverCallback callback;
    private TCPConnection conn;

    public TCPReceiverThread(Socket socket, TCPReceiverCallback cb, TCPConnection conn) throws IOException {
        this.socket = socket;
        this.callback = cb;
        this.conn = conn;
        din = new DataInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        int dataLength;

        while (true) {
            try {
                dataLength = din.readInt();
                byte[] data = new byte[dataLength];
                din.readFully(data, 0, dataLength);
//                System.arraycopy(socket.getInetAddress().getAddress(), 0, data, dataLength, 4);
                callback.invoke(data, conn);
            } catch (EOFException eof) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
