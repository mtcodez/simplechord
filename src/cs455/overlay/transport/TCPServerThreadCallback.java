package cs455.overlay.transport;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by mattluo on 2/14/15.
 */
public interface TCPServerThreadCallback {
    public void serverSocketListenCallback(Socket sock) throws IOException, InterruptedException;
}
