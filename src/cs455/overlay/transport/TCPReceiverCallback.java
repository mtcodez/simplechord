package cs455.overlay.transport;

import java.io.IOException;

/**
 * Created by mattluo on 2/10/15.
 */
public interface TCPReceiverCallback {
    public void invoke(byte[] data, TCPConnection conn) throws IOException, InterruptedException;
}
