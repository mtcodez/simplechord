package cs455.overlay.transport;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.*;

/**
 * Created by mattluo on 2/9/15.
 */
public class TCPConnection {
//    private TCPReceiverThread receiver;
//    private TCPSender sender;
    private Socket sock;
    private DataOutputStream dout;
    private TCPReceiverThread receiver;
    private int localPort;

    public TCPConnection(Socket sock, TCPReceiverCallback cb) throws IOException, InterruptedException {
        this.sock = sock;
        this.dout = new DataOutputStream(sock.getOutputStream());

        receiver = new TCPReceiverThread(sock, cb, this);
        startReceiver();
//        this.sender   = new TCPSender(sock);
    }
    public synchronized void setLocalPort(int port) {
        localPort = port;
    }
    public synchronized int getLocalPort() {
        return localPort;
    }
    private void startReceiver() {
        receiver.start();
    }
    public Socket getSock() {
        return sock;
    }
//    public byte[] getAddr() {
//        return sock.getInetAddress().getAddress();
//    }
    public String getLocalAddr() throws UnknownHostException {
        InetSocketAddress localAddr = (InetSocketAddress)sock.getLocalSocketAddress();
        return localAddr.getAddress().getHostAddress();
    }
    public String getRemoteAddr() {
        InetSocketAddress remoteAddr = (InetSocketAddress)sock.getRemoteSocketAddress();
        return remoteAddr.getAddress().getHostAddress();
    }
//    public int getPort() {
//        return sock.getLocalPort();
//    }

    public synchronized void send(byte[] data) throws IOException {
        int dataLength = data.length;
        try {
            dout.writeInt(dataLength);
            dout.write(data, 0, dataLength);
            dout.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
//        dout.close();
    }

}
