package cs455.overlay.transport;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by mattluo on 1/31/15.
 * The design of cache can be as simple as a hash table
 * And we maintain this hash table to keep track and store
 * those connections we established.
 * This way, we don't have to establish new connections everytime.
 */
public class TCPConnectionsCache {
    private TreeMap<Integer, TCPConnection> data;

    public TCPConnectionsCache() {
        data = new TreeMap<Integer, TCPConnection>();
    }
    public void add(Integer id, TCPConnection conn) {
        data.put(id, conn);
    }
    public TCPConnection get(Integer id) {
        return data.get(id);
    }
}
