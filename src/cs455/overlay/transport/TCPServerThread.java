package cs455.overlay.transport;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by mattluo on 1/31/15.
 */
public class TCPServerThread extends Thread {
    private ServerSocket server;
    private TCPReceiverCallback callback;
    private int port;
    public TCPServerThread(int port, TCPReceiverCallback cb) throws IOException {
        this.server = new ServerSocket(port);
        this.port = server.getLocalPort();
        this.callback = cb;
    }
    public int getPort() {
        return port;
    }
    public void run() {
        try {
            while (true) {
                Socket sock = server.accept();
                System.out.println("server thread accept()");
                byte[] data = new byte[1];
                data[0] = 0x0f;
                callback.invoke(data, new TCPConnection(sock, callback));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
