package cs455.overlay.routing;

import java.util.ArrayList;

/**
 * Created by mattluo on 1/31/15.
 */
public class RoutingTable {
    public RoutingTable(int sz, Integer id) {
        this.size = sz;
        this.table = new RoutingEntry[sz];
        this.id = id;
    }
    public synchronized void setTable(int index, RoutingEntry entry) {
        table[index] = entry;
    }
    public synchronized RoutingEntry searchEntry(Integer id) {
        for (int i = 0; i < size; i++) {
            if (table[i].getId().equals(id)) {
                return table[i];
            }
        }
        return null;
    }
    public synchronized RoutingEntry getBiggestOneNotExceeding(Integer id) {
        int max=0;
        for (int i = 0; i < size; i++) {
            if (table[i].getId() > table[max].getId() && table[i].getId() <= id) {
                max = i;
            }
        }return table[max];
    }
    public synchronized RoutingEntry getTableEntry(int index) {
        return table[index];
    }
    public void printRoutingTable() {
        for (int i = 0; i < size; i++) {
            System.out.println(table[i].getId());
        }
    }
    public Integer getId() {
        return id;
    }
    private Integer id;
    private int size;
    //private ArrayList<RoutingEntry> table;
    private RoutingEntry[] table;

}
