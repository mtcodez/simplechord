package cs455.overlay.routing;

import cs455.overlay.transport.TCPConnection;

/**
 * Created by mattluo on 1/31/15.
 */
public class RoutingEntry {
    private Integer id;
    private TCPConnection connection;
    public RoutingEntry(Integer id, TCPConnection connection) {
        this.id = id;
        this.connection = connection;
    }
    public synchronized void setId(Integer id) {
        this.id = id;
    }
    public synchronized Integer getId() {
        return id;
    }
    public synchronized  void setConnection(TCPConnection conn) {
        this.connection = conn;
    }
    public synchronized TCPConnection getConnection() {
        return connection;
    }
}
