package cs455.overlay.util;

import cs455.overlay.node.Registry;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Created by mattluo on 1/31/15.
 */
public class InteractiveCommandParser extends Thread {
    private Registry reg;
    public InteractiveCommandParser(Registry reg) {
        this.reg = reg;
    }
    public void run() {
        Scanner s = new Scanner(System.in);
        String line = null;
        while(true) {
            line = s.nextLine();
            String[] tokens = line.split(" ");
            if (tokens[0].equals("setup-overlay")) {
                if (tokens.length == 1) {
                    System.out.println("Invalid input. Should be setup-overly num.");
                    continue;
                }
                System.out.println("NR:" + tokens[1]);
                try {
                    reg.setupOverlay(Integer.parseInt(tokens[1]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (line.equals("list-messaging-nodes")) {
                try {
                    reg.listMessagingNodes();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            } else  if (line.equals("list-routing-tables")) {
                reg.listRoutingTables();
            } else if (tokens[0].equals("start")) {
                try {
                    reg.startSendingPackets(Integer.parseInt(tokens[1]));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Invalid. Try again.");
                continue;
            }
        }
    }
}
