package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by mattluo on 1/31/15.
 */
public class RegistryRequestsTaskInitiate implements Event {
    private byte[] data;
    public RegistryRequestsTaskInitiate(int num) throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        dout.writeByte(getType().getNumVal());
        dout.writeInt(num);
        dout.flush();

        this.data = baOutputStream.toByteArray();
        dout.close();
    }
    @Override
    public Protocol getType() {
        return Protocol.REGISTRY_REQUESTS_TASK_INITIATE;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
