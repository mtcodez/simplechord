package cs455.overlay.wireformats;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * Created by mattluo on 1/31/15.
 */
public class OverlayNodeSendsRegistration implements Event {
    private byte[] data;

    public OverlayNodeSendsRegistration(Socket sock, int port) throws IOException {
        // ip port
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        String inetaddr = InetAddress.getLocalHost().getHostAddress();
        dout.writeByte((byte)getType().getNumVal()); // TYPE
        System.out.println("TYpe:" + getType().getNumVal());
        dout.writeByte(inetaddr.length());       // inetaddress length
        dout.write(inetaddr.getBytes(), 0, inetaddr.length());              // inetaddress
        dout.writeInt(port);                  // port
        dout.flush();

        this.data = baOutputStream.toByteArray();
        dout.close();
    }

    @Override
    public Protocol getType() {
        return Protocol.OVERLAY_NODE_SENDS_REGISTRATION;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
