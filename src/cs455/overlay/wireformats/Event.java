package cs455.overlay.wireformats;

/**
 * Created by mattluo on 1/31/15.
 */
public interface Event {
    public Protocol getType();
    public byte[] getBytes();
}
