package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Created by mattluo on 1/31/15.
 */
public class NodeReportsOverlaySetupStatus implements Event{
    private byte[] data;
    public NodeReportsOverlaySetupStatus(int s) throws IOException {
        // ip port
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));

        dout.writeByte(getType().getNumVal());
        dout.writeInt(s);
        String info = new String("Info");
        dout.writeByte(info.length());
        dout.write(info.getBytes());
        dout.flush();
        this.data = baOutputStream.toByteArray();
        dout.close();
    }
    @Override
    public Protocol getType() {
        return Protocol.NODE_REPORTS_OVERLAY_SETUP_STATUS;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
