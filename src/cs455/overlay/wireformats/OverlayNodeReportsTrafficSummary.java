package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by mattluo on 1/31/15.
 */
public class OverlayNodeReportsTrafficSummary implements Event {
    private byte[] data;

    public OverlayNodeReportsTrafficSummary(int id, int sent, int recv, int relayed, long sumSent, long sumRcvd ) throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        dout.writeByte((byte)getType().getNumVal());
        dout.writeInt(id);
        dout.writeInt(sent);
        dout.writeInt(relayed);
        dout.writeLong(sumSent);
        dout.writeInt(recv);
        dout.writeLong(sumRcvd);
        dout.flush();
        this.data = baOutputStream.toByteArray();
        dout.close();
    }

    @Override
    public Protocol getType() {
        return Protocol.OVERLAY_NODE_REPORTS_TRAFFIC_SUMMARY;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
