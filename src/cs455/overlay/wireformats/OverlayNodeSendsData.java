package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by mattluo on 1/31/15.
 */
public class OverlayNodeSendsData implements Event {
    private byte[] data;
    private int payload;
    public OverlayNodeSendsData(Integer dest, Integer src) throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));

        dout.writeByte(getType().getNumVal());
        dout.writeInt(dest);
        dout.writeInt(src);
        Random rand = new Random();
        payload = rand.nextInt();
        dout.writeInt(payload);
//        dout.writeInt(info.length());
        dout.writeInt(0); // source has no dissemination
//        dout.write(info.getBytes());
        dout.flush();
        this.data = baOutputStream.toByteArray();
        dout.close();
    }
    public int getPayload() {
        return payload;
    }
    @Override
    public Protocol getType() {
        return Protocol.OVERLAY_NODE_SENDS_DATA;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
