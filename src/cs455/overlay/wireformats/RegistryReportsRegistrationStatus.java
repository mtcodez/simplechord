package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by mattluo on 1/31/15.
 */
public class RegistryReportsRegistrationStatus implements Event {
    private byte[] data;

    public RegistryReportsRegistrationStatus(int status, int nodenum) throws IOException {
        // ip port
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));

        dout.writeByte((byte)getType().getNumVal()); // TYPE
        dout.writeInt(status);

        String info = null;
        // success
        if (status != -1) {
            info = new String("Registration request successful. The number of messaging nodes currently constituting the overlay is (" + nodenum + ")");
        }
        // failed
        else {
            info = new String("failed.");
        }
        dout.writeBytes(info);
        dout.flush();

        this.data = baOutputStream.toByteArray();
        dout.close();
    }

    @Override
    public Protocol getType() {
        return Protocol.REGISTRY_REPORTS_REGISTRATION_STATUS;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
