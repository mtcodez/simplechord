package cs455.overlay.wireformats;

/**
 * Created by mattluo on 1/31/15.
 * EventFactory is a global resource manager, using Singleton pattern.
 */
public class EventFactory {
    private static EventFactory ourInstance = new EventFactory();

    public static EventFactory getInstance() {
        return ourInstance;
    }

    private EventFactory() {
    }
}
