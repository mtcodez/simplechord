package cs455.overlay.wireformats;

/**
 * Created by mattluo on 1/30/15.
 * Protocol Number
 */
public enum Protocol {
    OVERLAY_NODE_SENDS_REGISTRATION(2),
    REGISTRY_REPORTS_REGISTRATION_STATUS(3),
    REGISTRY_SENDS_NODE_MANIFEST(4),
    NODE_REPORTS_OVERLAY_SETUP_STATUS(5),
    REGISTRY_REQUESTS_TASK_INITIATE(6),
    OVERLAY_NODE_SENDS_DATA(7),
    OVERLAY_NODE_REPORTS_TASK_FINISHED(8),
    REGISTRY_REQUESTS_TRAFFIC_SUMMARY(9),
    OVERLAY_NODE_REPORTS_TRAFFIC_SUMMARY(10);

    private int numVal;

    Protocol(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}
