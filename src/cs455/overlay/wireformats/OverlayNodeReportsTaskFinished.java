package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

/**
 * Created by mattluo on 1/31/15.
 */
public class OverlayNodeReportsTaskFinished implements Event {
    private byte[] data;
    public OverlayNodeReportsTaskFinished(String ip, int port, Integer id) throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        dout.writeByte((byte)getType().getNumVal());
        dout.write(ip.getBytes());
        dout.writeInt(port);
        dout.writeInt(id);
        dout.flush();
        this.data = baOutputStream.toByteArray();
        dout.close();
    }
    @Override
    public Protocol getType() {
        return Protocol.OVERLAY_NODE_REPORTS_TASK_FINISHED;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
