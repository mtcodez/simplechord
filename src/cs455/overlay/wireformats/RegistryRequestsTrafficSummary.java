package cs455.overlay.wireformats;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Created by mattluo on 1/31/15.
 */
public class RegistryRequestsTrafficSummary implements Event {
    private byte[] data;
    public RegistryRequestsTrafficSummary() throws IOException {
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        dout.writeByte((byte)getType().getNumVal());
        dout.flush();
        this.data = baOutputStream.toByteArray();
        dout.close();
    }
    @Override
    public Protocol getType() {
        return Protocol.REGISTRY_REQUESTS_TRAFFIC_SUMMARY;
    }

    @Override
    public byte[] getBytes() {
        return data;
    }
}
