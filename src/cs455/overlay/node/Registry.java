package cs455.overlay.node;

import cs455.overlay.routing.RoutingEntry;
import cs455.overlay.routing.RoutingTable;
import cs455.overlay.transport.TCPConnection;
import cs455.overlay.transport.TCPReceiverCallback;
import cs455.overlay.util.InteractiveCommandParser;
import cs455.overlay.util.StatisticsCollectorAndDisplay;
import cs455.overlay.util.SummationEntry;
import cs455.overlay.wireformats.Protocol;
import cs455.overlay.wireformats.RegistryReportsRegistrationStatus;
import cs455.overlay.wireformats.RegistryRequestsTaskInitiate;
import cs455.overlay.wireformats.RegistryRequestsTrafficSummary;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mattluo on 1/30/15.
 */
public class Registry implements TCPReceiverCallback{
    private int port;
    private List<TCPConnection> connlist;
    private TreeMap<Integer, TCPConnection> connmap;
    private ArrayList<RoutingTable> routingTable;
    private AtomicInteger statusCount;
    private AtomicInteger taskFinishedCount;
    private AtomicInteger reportSentCount;
    private ArrayList<SummationEntry> sumTable;

    public Registry(int port) throws IOException,InterruptedException {
        this.port = port;
        connlist = new ArrayList<TCPConnection>();
        connmap = new TreeMap<Integer, TCPConnection>();
        routingTable = new ArrayList<RoutingTable>();
        statusCount = new AtomicInteger(0);
        taskFinishedCount = new AtomicInteger(0);
        reportSentCount = new AtomicInteger(0);
        sumTable = new ArrayList<SummationEntry>();
    }

    /**
     * Set up ServerSocket and listen for MessagingNode
     * connections.
     *
     * @throws IOException
     */
    public void listen() throws IOException,InterruptedException {
        ServerSocket listener = new ServerSocket();
        try {
            SocketAddress addr = new InetSocketAddress(port);
            listener.bind(addr);

            Socket sock = null;
            while (true) {
                sock = listener.accept(); // blocking HERE!!!
                TCPConnection conn = new TCPConnection(sock, this);

                // assign a unique number for the connection


                //connmap.put(num, conn);
                //System.out.println(connmap.keySet());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                listener.close();
            } catch (IOException ioe) {
                // ignore
            }
        }
    }

    public static void main(String[] args) throws IOException,InterruptedException {
        if(args.length != 1) {
            System.err.println("Invalid argument.\n port.");
            return;
        }
        int port = Integer.parseInt(args[0]);



        Registry registry = new Registry(port);

        InteractiveCommandParser parser = new InteractiveCommandParser(registry);
        parser.start();
        StatisticsCollectorAndDisplay stat = new StatisticsCollectorAndDisplay();
        stat.start();

        registry.listen(); // blocking HERE

    }
    private void reportStatus(int status, TCPConnection conn) throws IOException {
        byte[] reportStatusData = new RegistryReportsRegistrationStatus(status, connmap.keySet().size()).getBytes();
        conn.send(reportStatusData);
    }

    /**
     * Check the validation of the incoming registration.
     * And add information to a hashmap.
     *
     * @param data  the byte array received from MessagingNode
     */
    private void dealRegistration(byte[] data, TCPConnection conn) throws IOException {
        // check the information with OVERLAY_NODE_SENDS_REGISTRATION
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));

        // skip the type field
        int type = din.read();

        // read address
        int ipLength = din.read();
                byte[] addr  = new byte[ipLength];
        din.readFully(addr);

                // read port
        int port     = din.readInt();

        din.close();

        // store the corresponding TCPConnection
        // And assign a unique identifier number
        Random rand = new Random();
        Integer num;
        do {
            num = rand.nextInt(128);
        } while(connmap.containsKey(num));
        conn.setLocalPort(port);
        connmap.put(num, conn);

        int status = 0;
        //if (!new String(addr).equals(new String(conn.getAddr()))) {
        String addrStr = new String(addr);
        if (!addrStr.equals(conn.getRemoteAddr())) {
//            System.out.println("Address different.");
//            System.out.println("addr:" +  addrStr + " remote:" + conn.getRemoteAddr());
            status = -1;
        } else {
//            System.out.println("New Node added to registry.");
            status = num;
        }
        status = num;
//        System.out.println("connmap:" + connmap);
        reportStatus(status, conn);
    }

    private void dealDeregistration(byte[] data) {

    }

    public void setupOverlay(int NR) throws IOException {
//        Map<Integer, TCPConnection> treeMap = new TreeMap<Integer, TCPConnection>(connmap);
        List<Map.Entry<Integer, TCPConnection>> entryList = new ArrayList<Map.Entry<Integer, TCPConnection>>(connmap.entrySet());
        // iterate all connections and generate manifests for them
        for (Map.Entry<Integer, TCPConnection> entry: entryList) {
            TCPConnection conn = entry.getValue();
            conn.send(generateManifest(entry, entryList, NR));
            //System.out.println(entry.getKey());
        }
    }

    /**
     * Given id and the connection map,
     * returns the corresponding manifest.
     */
    private byte[] generateManifest(Map.Entry<Integer, TCPConnection> entry, List<Map.Entry<Integer, TCPConnection>> entryList, int NR) throws IOException {
        byte[] data;
        int size = entryList.size();
//        int size = NR;
        ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
        DataOutputStream dout =
                new DataOutputStream(new BufferedOutputStream(baOutputStream));
        // write type
        dout.writeByte((byte) Protocol.REGISTRY_SENDS_NODE_MANIFEST.getNumVal());
        // write routing table size
        if (size >= NR) {
            size = NR;
        }
        dout.writeByte(size);
        //System.out.println("NR:" + size % 3);
//        System.out.println("Table ==>" + entry.getKey());
        RoutingTable table = new RoutingTable(size, entry.getKey());
        for (int i = 0; i < size; i++) {
            // when size is 0, just break
            // zero means it wraps back to itself
//            if (size % 3 == 0) {
//                break;
//            }

            int index = entryList.indexOf(entry);
            int suc = (index + (int)Math.pow(2,i)) % entryList.size();
            if (suc == index) {
                continue;
            }
            Map.Entry<Integer, TCPConnection> next = entryList.get(suc);
            // id
            dout.writeInt(next.getKey());
            // address length
            dout.writeByte(next.getValue().getRemoteAddr().length());
            // address
            dout.write(next.getValue().getRemoteAddr().getBytes());
//            System.out.println("id:" + next.getKey() + " address:" + next.getValue().getRemoteAddr());
            // port
            dout.writeInt(next.getValue().getLocalPort());
//            System.out.println("port:" + next.getValue().getLocalPort());
//            routingTable.get(entry.getKey()).add(entryList.get(suc).getKey());
            table.setTable(i, new RoutingEntry(next.getKey(), null));
        }
        routingTable.add(table);
        dout.writeByte(entryList.size());
        for (Map.Entry<Integer, TCPConnection> en: entryList) {
            dout.writeInt(en.getKey());
        }
        dout.close();
        data = baOutputStream.toByteArray();
        return data;
    }
    public void listMessagingNodes() throws UnknownHostException {
        System.out.println("\n\nList MessagingNodes");
        for (Integer id: connmap.keySet()) {
            System.out.println("MessagingNode:" + id + "\thost:" + connmap.get(id).getRemoteAddr() + "\tport:" + connmap.get(id).getLocalPort());
        }
    }
    public void listRoutingTables() {
        for (RoutingTable rt: routingTable) {
            System.out.println("\n\n Routing table for Node:" + rt.getId());
            rt.printRoutingTable();
        }

    }
    public void startSendingPackets(int number) throws IOException {
        for (Integer id: connmap.keySet()) {
            connmap.get(id).send(new RegistryRequestsTaskInitiate(number).getBytes());
        }
    }
    /** Deal with situations when Node sends information
     *
     * @param data  the byte array received from MessagingNode
     */
    @Override
    public void invoke(byte[] data, TCPConnection conn) throws IOException {
//        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));

        byte type = din.readByte();
        //System.out.println(type);
        switch (type) {
            case (byte)0x02: // OVERLAY_NODE_SENDS_REGISTRATION
                dealRegistration(data, conn);
                break;
            case (byte)0x05: // NODE_REPORTS_OVERLAY_SETUP_STATUS
                int s = din.readInt();
             //                if (s != -1) {
                statusCount.addAndGet(1);
//                }
                if (statusCount.get() == connmap.keySet().size()) {
                    System.out.println("\nRegistry now ready to initiate tasks.");
                }
                break;
            case (byte)0x08: // OVERLAY_NODE_REPORTS_TASK_FINISHED
                taskFinishedCount.addAndGet(1);
                if (taskFinishedCount.get() == connmap.keySet().size()) {
                    // Send summation request.
                    System.out.println("All tasks completed.");
                    requestReport();
                }
                break;
            case (byte)0x0a:
//                System.out.println("received summation.");
                processSummation(data);
                reportSentCount.addAndGet(1);
                if (reportSentCount.get() == connmap.keySet().size()) {
                    printSummation();
//                    reportSentCount.set(0);
                }
                break;
        }
        in.close();
    }

    private synchronized void processSummation(byte[] data) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));
        din.readByte();
        int id = din.readInt();
        int sent = din.readInt();
        int relayed = din.readInt();
        long sumSent = din.readLong();
        int recvd = din.readInt();
        long sumRecvd = din.readLong();
        sumTable.add(new SummationEntry(sent,recvd,relayed,id,sumSent, sumRecvd));
    }

    private synchronized void printSummation() {
        int st =0 ,rcvd =0,rld=0;
        long sums=0, sumr=0;
        System.out.println("sumTable:" + sumTable.size());
        System.out.println("\n\n==============");
        System.out.println("Node\tSent\tReceived\tRelayed\tSum Values Sent\ttSum Values Received");
        for (SummationEntry s: sumTable) {
            System.out.println(s.id + "\t" + s.sent + "\t" + s.rcvd +"\t"+ s.relayed +"\t"+s.sumSent+"\t"+s.sumReceived);
            st += s.sent;
            rcvd += s.rcvd;
            rld += s.relayed;
            sums += s.sumSent;
            sumr += s.sumReceived;
        }
        System.out.println("Sum\t" + st + "\t" + rcvd +"\t"+ rld +"\t"+sums+"\t"+sumr);


    }

    private void requestReport() throws IOException {
        for (Integer id: connmap.keySet()) {
            connmap.get(id).send(new RegistryRequestsTrafficSummary().getBytes());
        }
    }
}
