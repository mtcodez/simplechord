package cs455.overlay.node;

import cs455.overlay.routing.RoutingEntry;
import cs455.overlay.routing.RoutingTable;
import cs455.overlay.transport.*;
import cs455.overlay.wireformats.*;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by mattluo on 1/30/15.
 * Should have a main function and others
 */
public class MessagingNode implements TCPReceiverCallback, TCPServerThreadCallback {
    private String host;
    private int    port;
    private int listenPort;
    private List<TCPConnection> peerConnectionList;

    private TCPConnection registryConnection;
    private TCPServerThread serverThread;

    private EventFactory factory;
    private TCPConnection listenConnection;
    private RoutingTable table;
    private Integer id;
    private int[] peerList;
    // statistics
    private AtomicInteger packetsSent;
    private AtomicInteger packetsReceived;
    private AtomicInteger packetsRelayed;
    private AtomicLong sumSent;
    private AtomicLong sumReceived;

    public MessagingNode(String host, int port) throws IOException,InterruptedException {
        this.host = host;
        this.port = port;

        factory = EventFactory.getInstance();
        peerConnectionList = new ArrayList<TCPConnection>();

        packetsSent = new AtomicInteger(0);
        packetsReceived = new AtomicInteger(0);
        packetsRelayed = new AtomicInteger(0);
        sumSent = new AtomicLong(0);
        sumReceived = new AtomicLong(0);

        Socket sock = null;
        try {
            sock = new Socket(host, port);
            registryConnection = new TCPConnection(sock, this);
//            peerSocketList.add(connection);
            // Registration
            //OverlayNodeSendsRegistration ev = new OverlayNodeSendsRegistration(sock, port);
//            registryConnection.send(ev.getBytes());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendRegistraion() throws IOException {
        OverlayNodeSendsRegistration ev = new OverlayNodeSendsRegistration(registryConnection.getSock(), listenPort);
        registryConnection.send(ev.getBytes());
    }
    private void setupServerSocket() throws IOException {
        serverThread = new TCPServerThread(0, this);
        listenPort = serverThread.getPort();
        System.out.println("MNode Listen port:" + listenPort);
        serverThread.start();
    }

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.err.println("Invalid number of argument. use address port.");
        }

        String host = args[0];
        int    port = Integer.parseInt(args[1]);

        try {
            MessagingNode mnode = new MessagingNode(host, port);
            mnode.setupServerSocket();
            mnode.sendRegistraion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int processManifest(byte[] manifest) throws IOException, InterruptedException {
        int status = this.id;

        ByteArrayInputStream in = new ByteArrayInputStream(manifest);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));

        din.readByte();
        int NR = din.readByte();
        table = new RoutingTable(NR, id);
        for (int i = 0; i < NR; i++) {
//            if (NR % 3 == 0) {
//                break;
//            }
            Integer id = din.readInt();
            int ipLength = din.readByte();
            byte[] ipAddr = new byte[ipLength];
            din.read(ipAddr); // ip address

            // add to routing table
            int port = din.readInt();    // port
            //System.out.println("port in manifest:" + port);
            String host = new String(ipAddr); //InetAddress.getByAddress(ipAddr).getHostAddress();
            Socket sock=null;
            try {
                sock = new Socket(host, port);
//                System.out.println("Connect to socket id:" + id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            TCPConnection conn = new TCPConnection(sock, this);
            table.setTable(i, new RoutingEntry(id, conn));
        }
        // list of nodes' ids
        int nodeNum = din.readByte();
        peerList = new int[nodeNum];
        for (int j = 0; j < nodeNum; j++) {
            peerList[j] = din.readInt();
        }

        return status;

    }
    public void sendSetupStatus(int status) {

    }
    public void startSendingPackets(int num) throws IOException {
//        System.out.println("send packets of number:" + num);
        Random rand = new Random();
        int targ;
        do {
            targ = rand.nextInt(peerList.length);
        } while (peerList[targ] == this.id);

        RoutingEntry search = table.searchEntry(peerList[targ]);
//        System.out.print("from: " + this.id + " to:" + peerList[targ]);
        if (search != null) {
//            System.out.println(" through:" + search.getId());
        } else {
//            System.out.println(" through:" + table.getBiggestOneNotExceeding(targ).getId());
        }
        if (search != null) {
            for (int j = 0; j < num; j++) {
                packetsSent.addAndGet(1);
                OverlayNodeSendsData sent = new OverlayNodeSendsData(peerList[targ], this.id);
                sumSent.addAndGet(sent.getPayload());
                search.getConnection().send(sent.getBytes());
            }
        } else { // not in routing table
            for (int j = 0; j < num; j++) {
                packetsSent.addAndGet(1);
                OverlayNodeSendsData sent =new OverlayNodeSendsData(peerList[targ], this.id);
                sumSent.addAndGet(sent.getPayload());
                table.getBiggestOneNotExceeding(targ).getConnection().send(sent.getBytes());
            }
        }
        System.out.println("=== >> Task finished.");
        System.out.println("packets sent:" + packetsSent);
        System.out.println("packets received:" + packetsReceived);
        System.out.println("packets relayed:" + packetsRelayed);
        registryConnection.send(new OverlayNodeReportsTaskFinished(InetAddress.getLocalHost().getHostAddress(), this.listenPort, this.id).getBytes());
    }

    /**
     *
     * @param data
     * @param conn
     * @throws IOException
     */
    @Override
    public void invoke(byte[] data, TCPConnection conn) throws IOException, InterruptedException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));
        byte type = din.readByte();
        switch (type) {
            case (byte)0x03: // REGISTRY_REPORTS_REGISTRATION_STATUS
                int status = din.readInt();
                if (status == -1) {
                    System.out.println("MessagingNode: Registration successful!");
                    id = -1;
                } else {
                    System.out.println("MessagingNode: Registration FAILED!");
                    id = status;
                }
                break;
            case (byte)0x04: // REGISTRY_SENDS_NODE_MANIFEST
                System.out.println("Receive the manifest.");
                int s = processManifest(data);
                conn.send(new NodeReportsOverlaySetupStatus(s).getBytes());
                System.out.println("Send Status.");
                break;
            case (byte)0x06: // REGISTRY_REQUESTS_TASK_INITIATE
                System.out.println("Recv Task init. id:" + this.id);
                startSendingPackets(din.readInt());
                break;
            case (byte)0x07:
//                System.out.println("Get Send Packets." + " dest:" + din.readInt() + " src:" + din.readInt());
                checkPacketOrRelay(data);
                break;
            case (byte)0x09:
                System.out.println("send summation");
                registryConnection.send(new OverlayNodeReportsTrafficSummary(this.id, packetsSent.get(),  packetsReceived.get(), packetsRelayed.get(), sumSent.get(), sumReceived.get()).getBytes());
                break;
            case (byte)0x0f:  // connect to nodes in routing table
                System.out.println("Added to peer list. peer list:" + peerConnectionList.size());
                peerConnectionList.add(conn);
                break;
        }
    }

    private  void checkPacketOrRelay(byte[] data) throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        DataInputStream din =
                new DataInputStream(new BufferedInputStream(in));
        din.readByte();
        int dest = din.readInt();
        int src  = din.readInt();
        int payload = din.readInt();
        int length = din.readInt();
        byte[] dissemination = new byte[length];
        din.read(dissemination);

        if (dest == this.id) {
            // statistics
            packetsReceived.addAndGet(1);
            sumReceived.addAndGet(payload);
            return;
        } else { // relay
            packetsRelayed.addAndGet(1);
            byte[] send;
            ByteArrayOutputStream baOutputStream = new ByteArrayOutputStream();
            DataOutputStream dout =
                    new DataOutputStream(new BufferedOutputStream(baOutputStream));

            dout.writeByte((byte)Protocol.OVERLAY_NODE_SENDS_DATA.getNumVal());
            dout.writeInt(dest);
            dout.writeInt(src);
            dout.writeInt(payload);

            dout.writeInt(dissemination.length+4);
            dout.write(dissemination);
            dout.writeInt(this.id);

            dout.flush();
            send = baOutputStream.toByteArray();
            dout.close();
            table.getBiggestOneNotExceeding(dest).getConnection().send(send);
        }
    }

    /**
     * Listen for connections from peer MessagingNodes
     * @param sock socket returned from ServerSocket
     */
    @Override
    public void serverSocketListenCallback(Socket sock) throws IOException, InterruptedException {
//        peerSocketList.add(new TCPConnection(sock, this));
//        System.out.println("Get the neighbor.." + table);
        System.out.println("Added to peer list. peer list:" + peerConnectionList.size());

        peerConnectionList.add(new TCPConnection(sock, this));
    }
}
